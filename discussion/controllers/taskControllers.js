const Task = require('../models/taskSchema');
// Retrieving all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	});
};

// Creating a task
module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	});

	return newTask.save().then((task, err) => {
		if (err) {
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if (err) {
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})

}

// Update Task
module.exports.updateTask = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, err) => {
		if (err) {
			console.log(err)
			return false
		}

		result.name = reqBody.name
			return result.save().then((updateTask, saveErr) => {
				if (saveErr) {
					console.log(saveErr)
					return false
				} else {
					return updateTask
				}
			})
	})
}

// Activity
// 1. Create a controller function for retrieving a specific task
module.exports.getSpecificTask = (taskId) => {
    return Task.findById(taskId).then((task,err) => {
        if(err){
            console.log(err)
            return false
        } else {
            return task
        }
    })
}
// 2. Create a controller function for changing the status of a task to complete
module.exports.changeStatus = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if (err) {
			console.log(err)
			return false
		}

		result.status = "complete";

			return result.save().then((changeStatus, saveErr) => {
				if (saveErr) {
					console.log(saveErr)
					return saveErr
				} else {
					result.status = "complete";
				}
			})
	})
}