const express = require('express');
const router = express.Router();
const taskControllers = require('../controllers/taskControllers');

router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Mini-Activity
// 1.Create a route for createTask controller, name your endpoint as /creatTask
// 2. Send your potman screenshot in hangouts
router.post('/createTask', (req, res) => {taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController))
});

// END OF SOLUTION

// Deleting
router.delete("/deleteTask/:id", (req, res) => {taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// Updating
router.put("/updateTask/:id", (req, res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// Activity
// 1. create a route for getting a specific task
router.get("/:id", (req, res) => {
	taskControllers.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Create a route to change the status of a tasks
router.put("/:id/complete", (req, res) => {
	taskControllers.changeStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router;